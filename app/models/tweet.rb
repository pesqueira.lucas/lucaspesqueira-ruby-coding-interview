# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user

  validates :body, presence: true, length: { maximum: 180 }
  validate :check_body_message, on: :create

  private

  def check_body_message
    tweets = user.tweets.where('body = ? AND created_at > ?', self.body, Date.today - 1.day)

    if tweets.present?
      errors.add(:body, message: 'This message was already posted today')

      return false
    end
  end
end
