# == Schema Information
#
# Table name: followed_users
#
#  id               :integer          not null, primary key
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  followed_user_id :integer          not null
#  follower_user_id :integer          not null
#
class FollowedUser < ApplicationRecord
  belongs_to :followed_user, class_name: 'User', foreign_key: 'followed_user_id'
  belongs_to :follower_user, class_name: 'User', foreign_key: 'follower_user_id'
end
