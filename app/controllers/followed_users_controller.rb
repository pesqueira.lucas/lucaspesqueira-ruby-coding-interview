class FollowedUsersController < ApplicationController
  def create
    FollowedUser.create(followed_user_id: params[:followed_user_id],
                        follower_user_id: current_user.id)

    redirect_to root_path
  end

  def destroy
    FollowedUser.find_by(followed_user_id: params[:id],
                         follower_user_id: current_user.id).destroy

    redirect_to root_path
  end
end
