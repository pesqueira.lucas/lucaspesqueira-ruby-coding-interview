class CreateFollowedUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :followed_users do |t|
      t.integer :follower_user_id, null: false
      t.integer :followed_user_id, null: false

      t.timestamps
    end
  end
end
