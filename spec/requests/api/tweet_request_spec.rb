require 'rails_helper'

RSpec.describe "API Tweets", type: :request do
  let(:response_body) { JSON.parse(response.body) }

  describe "#index" do
    context 'When there are tweets' do
      let!(:tweet_list) { create_list(:tweet, 2)}

      before do
        create_list(:tweet, 5)
      end

      it 'returns a successful response' do
        get api_tweets_path

        expect(response).to have_http_status(:success)
      end
    end
  end

  describe "#create" do
    context 'with valid parameters' do
      let(:user) { create(:user)}
      let(:valid_body) { 'This is a valid tweet' }

      it 'returns a successful response' do
        post api_tweets_path(user_id: user.id, body: valid_body)

        expect(response).to have_http_status(:success)
      end

      it 'creates a new tweet' do
        expect {
          post api_tweets_path(user_id: user.id, body: valid_body)
        }.to change(Tweet, :count).by(1)
      end

      context 'When the tweet is a duplicate before 24 hours' do
        let!(:tweet) { create(:tweet, body: valid_body, user: user) }

        it 'returns a successful response' do
          post api_tweets_path(user_id: user.id, body: valid_body)

          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'creates a new tweet' do
          expect {
            post api_tweets_path(user_id: user.id, body: valid_body)
          }.to change(Tweet, :count).by(0)
        end
      end

      context 'When the tweet is a duplicate yesterday' do
        let!(:tweet) { create(:tweet, body: valid_body, user: user, created_at: 2.day.ago) }

        it 'returns a successful response' do
          post api_tweets_path(user_id: user.id, body: valid_body)

          expect(response).to have_http_status(:success)
        end

        it 'creates a new tweet' do
          expect {
            post api_tweets_path(user_id: user.id, body: valid_body)
          }.to change(Tweet, :count).by(1)
        end
      end
    end

    context 'With invalid parameters' do
      let(:user) { create(:user)}

      context 'When the body is invalid' do
        let(:invalid_body) { '' }

        it 'returns an error response' do
          post api_tweets_path(user_id: user.id, body: invalid_body)

          expect(response).to have_http_status(:bad_request)
        end

        it 'does not create a new tweet' do
          expect {
            post api_tweets_path(user_id: user.id, body: invalid_body)
          }.to_not change(Tweet, :count)
        end
      end

      context 'When the tweet is invalid' do
        let(:body) { 'This is a valid tweet' }
        let!(:tweet1) { create(:tweet, user: user, body: body) }

        it 'returns an error response' do
          post api_tweets_path(user_id: user.id, body: body)

          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'does not create a new tweet' do
          expect {
            post api_tweets_path(user_id: user.id, body: body)
          }.to_not change(Tweet, :count)
        end
      end

      context 'when the body exeeds the maximum length' do
        let(:body) { 'This is not a valid tweet' * 100 }

        it 'returns an error response' do
          post api_tweets_path(user_id: user.id, body: body)

          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'does not create a new tweet' do
          expect {
            post api_tweets_path(user_id: user.id, body: body)
          }.to_not change(Tweet, :count)
        end
      end
    end
  end
end
