require 'rails_helper'

RSpec.describe FollowedUser, type: :model do
  subject(:followed_user) { create(:followed_user) }

  describe 'relationships' do
    it { is_expected.to belong_to(:followed_users) }
    it { is_expected.to belong_to(:follower_users) }
  end
end
