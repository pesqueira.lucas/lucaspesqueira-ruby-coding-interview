FactoryBot.define do
  factory :followed_user do
    followed_user_id { create(:user).id }
    follower_user_id { create(:user).id }
  end
end
